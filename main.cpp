#include <QtConcurrent/QtConcurrentMap>
#include <QVector>

auto map(const QString &x) -> QString {  // clazy:exclude=function-args-by-value
    return x;
}

void reduce(QString &result, const QString &partial) {  // clazy:exclude=function-args-by-value
    qDebug() << QStringLiteral("Parcial: %1").arg(partial);
    result = result+" "+partial;
}

auto main(int argc, char *argv[]) -> int
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    QVector<QString> vector {"o","melhor","curso","de","qt","do","Brasil","é","o","da","QMOB"};

    auto future = QtConcurrent::mappedReduced(vector, map, reduce);
    qDebug() << QStringLiteral("Resultado soma: %1").arg(future.result());
    return 0;
}
